package com.tuyano.springboot;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

public interface DaoMaster<T> extends Serializable
{
	public Optional<List<T>> getAll();
	public Optional<T> findById(long id);
	public Optional<List<T>> find(String name);
}
