package com.tuyano.springboot;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;


import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import com.tuyano.springboot.repositories.MyDataRepository;
import com.tuyano.springboot.utils.OptionalUtil;


@Controller
public class HeloController {
	
	@Autowired
	MyDataRepository repository;

	@Autowired
	private MyDataService service; 
//	@PersistenceContext
//	EntityManager entityManager;
//	MyDataDao dao;

	@PostConstruct
	public void init() {
//		dao = new MyDataDao(entityManager);
		
		MyData d1 = new MyData();
		d1.setName("tuyano");
		d1.setAge(123);
		d1.setMail("syoda@gmail.com");
		d1.setMemo("this is my data!");
		repository.saveAndFlush(d1);
		MyData d2 = new MyData();
		d2.setName("hanako");
		d2.setAge(15);
		d2.setMail("hanako@gmail.com");
		d2.setMemo("my girl friend.");
		repository.saveAndFlush(d2);
		MyData d3 = new MyData();
		d3.setName("sachiko");
		d3.setAge(37);
		d3.setMail("sachiko@gmail.com");
		d3.setMemo("my work friend...");
		repository.saveAndFlush(d3);
	
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView index(@ModelAttribute MyData mydata, ModelAndView mav) {
		mav.setViewName("index");
		mav.addObject("msg","this is sample content.");
		mav.addObject("formModel",mydata);
		
		Optional<List<MyData>> optLlist = service.getAll();
		List<MyData> list = optLlist.isPresent() ? optLlist.get() : new ArrayList<MyData>();
		mav.addObject("datalist",list);
		
		return mav;
	}

	@RequestMapping(value = "/", method= RequestMethod.POST)
	@Transactional(readOnly = false)
	public ModelAndView form(
			@ModelAttribute("formModel") MyData mydata,
			ModelAndView mav) {
		
		repository.saveAndFlush(mydata);
		return new ModelAndView("redirect:/");
	}

	@RequestMapping(value = "/find", method = RequestMethod.GET)
	public ModelAndView find(ModelAndView mav) {
		mav.setViewName("find");
		mav.addObject("msg","this is find page.");
		Optional<List<MyData>> optLlist = service.getAll();
		
		List<MyData> list = optLlist.isPresent() ? optLlist.get() : new ArrayList<MyData>();
		mav.addObject("datalist",list);
		
		return mav;
	}

	@RequestMapping(value = "/find", method = RequestMethod.POST)
	public ModelAndView search(@RequestParam String str, ModelAndView mav) {
		mav.setViewName("find");
		
		if (OptionalUtil.applyWithString(str).isPresent()) {
			mav.addObject("msg","「"+str+"」の検索結果");
			mav.addObject("value",str);
			Optional<List<MyData>> optLlist = service.find(str);
			List<MyData> list = optLlist.isPresent() ? optLlist.get() : new ArrayList<MyData>();
			mav.addObject("datalist",list);
		} else {
			mav = new ModelAndView("redirect:/find");
		}
		
		return mav;
	}
	
	@RequestMapping(value = "/edit/{id}", method= RequestMethod.GET)
	public ModelAndView edit(
			@ModelAttribute MyData mydata,
			@PathVariable int id,
			ModelAndView mav) {
		mav.setViewName("edit");
		mav.addObject("title","edit mydata.");
		MyData data = repository.findById((long)id);
		mav.addObject("formModel",data);
		return mav;
	}
	
	@RequestMapping(value = "/edit", method= RequestMethod.POST)
	@Transactional(readOnly = false)
	public ModelAndView update(
			@ModelAttribute MyData mydata,
			ModelAndView mav) {
		
		repository.saveAndFlush(mydata);
		return new ModelAndView("redirect:/");
	}


	
	
}
