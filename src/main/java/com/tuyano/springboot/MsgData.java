package com.tuyano.springboot;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
@Entity
@Table(name="msgdata")
public class MsgData {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column
	@NotNull
	private long id;
	
	@Column(nullable = false)
	private String title;
	
	@Column(nullable = false)
	@NotEmpty
	private String message;

	@Column(nullable = false)
	private long mydataId;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch=FetchType.EAGER)
	@JoinColumn(insertable=false, updatable=false, name = "mydataId")
	private MyData mydata;

	public MsgData() {
		super();
		this.mydata = new MyData();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public MyData getMyData() {
		return mydata;
	}

	public void setMyData(MyData mydata) {
		this.mydata = mydata;
	}

	public long getMydataId() {
		return mydataId;
	}

	public void setMydataId(long mydataId) {
		this.mydataId = mydataId;
	}
	
	
	
	
	
}
