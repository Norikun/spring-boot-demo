package com.tuyano.springboot;

import static org.mockito.Matchers.longThat;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.tuyano.springboot.repositories.MsgDataRepository;



@Controller
public class MsgDataController {
	
	@Autowired
	MsgDataRepository repository;
	
	@PersistenceContext
	EntityManager entityManager;
	MsgDataDao msgDao;
	MyDataDao myDao;

	@PostConstruct
	public void init() {
		System.out.println("##### here is MsgDataController init!");
		msgDao = new MsgDataDao(entityManager);
		myDao = new MyDataDao(entityManager);
	}
	
	@RequestMapping(value = "/msg", method = RequestMethod.GET)
	public ModelAndView msg(ModelAndView mav) {
		mav.setViewName("showMsgData");
		mav.addObject("title", "msg Sample");
		mav.addObject("msg","this is msg sample content.");
		
		MsgData msgdata = new MsgData();
		mav.addObject("formModel",msgdata);
		
		Optional<List<MsgData>> optList = msgDao.getAll();
		//List<MsgData> list = optList.isPresent() ? optList.get() : new ArrayList<MsgData>();
		List<MsgData> list = optList.orElse(new ArrayList<MsgData>());
		mav.addObject("datalist",list);
		
		return mav;
	}

	@RequestMapping(value = "/msg", method = RequestMethod.POST)
	@Transactional(readOnly = false)
	public ModelAndView msgForm(
			@Valid @ModelAttribute MsgData msgdata,
			HttpServletRequest request,
			Errors result,
			ModelAndView mav) {
		
		if (result.hasErrors()) {
		mav.setViewName("showMsgData");
		mav.addObject("title", "[ERROR]msg Sample");
		mav.addObject("msg","入力に誤りがあります。");
		return mav;
		} else {
			long mydataID = Long.parseLong(request.getParameter("mydataId"));
			MyData myData = myDao.findById(mydataID).get();
			msgdata.setMyData(myData);
			repository.saveAndFlush(msgdata);
			
			return new ModelAndView("redirect:/msg");
		}
	}
}
