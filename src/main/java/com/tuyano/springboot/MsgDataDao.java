package com.tuyano.springboot;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.criteria.*;

import org.springframework.stereotype.Repository;

import com.tuyano.springboot.utils.OptionalUtil;

@Repository
public class MsgDataDao implements DaoMaster<MsgData>{
	private static final long serialVersionUID = 1L;
	private EntityManager entityManager;
	private CriteriaBuilder builder;
	private CriteriaQuery<MsgData> query;
	private Root<MsgData> root;
	
	public MsgDataDao() {
		super();
	}

	public MsgDataDao(EntityManager manager) {
		this.entityManager = manager;
		this.builder = this.entityManager.getCriteriaBuilder();
		this.query = this.builder.createQuery(MsgData.class);
		this.root = this.query.from(MsgData.class);
	}

	@Override
	public Optional<List<MsgData>> getAll() {
//		CriteriaQuery<MsgData> query = entityManager.getCriteriaBuilder().createQuery(MsgData.class);
//		query.select(query.from(MsgData.class));
		
		query.select(root).orderBy(builder.desc(root.get("id")));
		List<MsgData> list = (List<MsgData>)entityManager.createQuery(query).getResultList();
		
		//TODO 必須か？
		entityManager.close();
		return OptionalUtil.apply(list);
	}

	@Override
	public Optional<MsgData> findById(long id) {
		query.select(root)
		.where(builder.equal(root.get("id"), id));
	
	MsgData data = (MsgData)entityManager.createQuery(query).getSingleResult();

	//TODO 必須か？
	entityManager.close();
	return OptionalUtil.apply(data);
	}

	@Override
	public Optional<List<MsgData>> find(String title) {
//		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
//		CriteriaQuery<MsgData> query = builder.createQuery(MsgData.class);
//		Root<MsgData> root = query.from(MsgData.class);
		query.select(root)
			.where(builder.equal(root.get("title"), title))
			.orderBy(builder.desc(root.get("id")));
		
		List<MsgData> list = (List<MsgData>)entityManager.createQuery(query).getResultList();

		//TODO 必須か？
		entityManager.close();
		return OptionalUtil.apply(list);
	}
	
	
	
	
	
	
	
}
