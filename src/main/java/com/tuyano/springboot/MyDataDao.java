package com.tuyano.springboot;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.criteria.*;

import org.springframework.stereotype.Repository;

import com.tuyano.springboot.utils.OptionalUtil;

@Repository
public class MyDataDao implements DaoMaster<MyData>{
	private static final long serialVersionUID = 1L;
	private EntityManager entityManager;
	private CriteriaBuilder builder;
	private CriteriaQuery<MyData> query;
	private Root<MyData> root;
	
	public MyDataDao() {
		super();
	}

	public MyDataDao(EntityManager manager) {
		this.entityManager = manager;
		this.builder = this.entityManager.getCriteriaBuilder();
		this.query = this.builder.createQuery(MyData.class);
		this.root = this.query.from(MyData.class);
	}

	@Override
	public Optional<List<MyData>> getAll() {
//		CriteriaQuery<MyData> query = entityManager.getCriteriaBuilder().createQuery(MyData.class);
//		query.select(query.from(MyData.class));
		
		query.select(root).orderBy(builder.desc(root.get("id")));
		List<MyData> list = (List<MyData>)entityManager.createQuery(query).getResultList();
		
		//TODO 必須か？
		entityManager.close();
		return OptionalUtil.apply(list);
	}

	@Override
	public Optional<MyData> findById(long id) {
		query.select(root)
		.where(builder.equal(root.get("id"), id));
	
	MyData data = (MyData)entityManager.createQuery(query).getSingleResult();

	//TODO 必須か？
	entityManager.close();
	return OptionalUtil.apply(data);
	}

	@Override
	public Optional<List<MyData>> find(String name) {
//		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
//		CriteriaQuery<MyData> query = builder.createQuery(MyData.class);
//		Root<MyData> root = query.from(MyData.class);
		query.select(root)
			.where(builder.equal(root.get("name"), name))
			.orderBy(builder.desc(root.get("id")));
		
		List<MyData> list = (List<MyData>)entityManager.createQuery(query).getResultList();

		//TODO 必須か？
		entityManager.close();
		return OptionalUtil.apply(list);
	}
	
	
	
	
	
	
	
}
