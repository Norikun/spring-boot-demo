package com.tuyano.springboot;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import org.springframework.stereotype.Service;

import com.tuyano.springboot.utils.OptionalUtil;

@Service
public class MyDataService {
//	private CriteriaBuilder builder;
//	private CriteriaQuery<MyData> query;
//	private Root<MyData> root;
	
	@PersistenceContext
	EntityManager entityManager;

//	{
//		builder = entityManager.getCriteriaBuilder();
//		query = builder.createQuery(MyData.class);
//		root = query.from(MyData.class);
//	}
//	public MyDataService() {
//		this.builder = entityManager.getCriteriaBuilder();
//		this.query = this.builder.createQuery(MyData.class);
//		this.root = this.query.from(MyData.class);
//	}

	
	public Optional<List<MyData>> getAll() {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<MyData> query = builder.createQuery(MyData.class);
		Root<MyData> root = query.from(MyData.class);
		
		query.select(root).orderBy(builder.desc(root.get("id")));
		List<MyData> list = (List<MyData>)entityManager.createQuery(query).getResultList();
		
		//TODO 必須か？
		entityManager.close();
		return OptionalUtil.apply(list);
	}

	
	public Optional<MyData> findById(long id) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<MyData> query = builder.createQuery(MyData.class);
		Root<MyData> root = query.from(MyData.class);

		query.select(root)
		.where(builder.equal(root.get("id"), id));
	
	MyData data = (MyData)entityManager.createQuery(query).getSingleResult();

	//TODO 必須か？
	entityManager.close();
	return OptionalUtil.apply(data);
	}

	
	public Optional<List<MyData>> find(String name) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<MyData> query = builder.createQuery(MyData.class);
		Root<MyData> root = query.from(MyData.class);

		query.select(root)
			.where(builder.equal(root.get("name"), name))
			.orderBy(builder.desc(root.get("id")));
		
		List<MyData> list = (List<MyData>)entityManager.createQuery(query).getResultList();

		//TODO 必須か？
		entityManager.close();
		return OptionalUtil.apply(list);
	}
	
	
	
	
	
	
	
}
