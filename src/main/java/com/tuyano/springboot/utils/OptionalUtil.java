package com.tuyano.springboot.utils;

import java.util.List;
import java.util.Optional;

/**
 * Option用ユーティリティクラス
 */
public class OptionalUtil {

    public static <A> Optional<A> apply(A value) {
        if(value != null) {
            return Optional.of(value);
        } else {
            return Optional.empty();
        }
    }

    public static <A> Optional<List<A>> apply(List<A> value) {
        if(value != null && value.size() != 0) {
            return Optional.of(value);
        } else {
            return Optional.empty();
        }
    }

    public static Optional<String> applyWithString(String value) {
        if(value != null && !value.equals("")) {
            return Optional.of(value);
        } else {
            return Optional.empty();
        }
    }
    
//    public static Optional<Object> applyWithObject(Object value) {
//        if(!value) {
//            return Optional.of(value);
//        } else {
//            return Optional.empty();
//        }
//    }

}
